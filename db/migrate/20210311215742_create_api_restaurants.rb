class CreateApiRestaurants < ActiveRecord::Migration[6.1]
  def change
    create_table :api_restaurants do |t|
      t.references :cuisine, null: false, foreign_key: { to_table: 'api_restaurants' }
      t.string :name, limit: 128
      t.integer :rating, limit: 1
      t.integer :distance
      t.decimal :price, precision: 8, scale: 2
    end
  end
end
