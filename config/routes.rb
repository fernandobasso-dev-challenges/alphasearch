Rails.application.routes.draw do
  namespace :api do
    get 'restaurants/search'
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'search', to: 'restaurants#search'
end
