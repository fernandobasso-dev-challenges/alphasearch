# Alpha Search Rails API

- [Alpha Search Rails API](#alpha-search-rails-api)
  - [Intro](#intro)
  - [Testing and Development](#testing-and-development)
  - [Create the PostgreSQL role and databases](#create-the-postgresql-role-and-databases)
  - [Import CSV data into devel database](#import-csv-data-into-devel-database)
  - [Local Server and Testing](#local-server-and-testing)
  - [Assumptions and further improvements](#assumptions-and-further-improvements)
    - [Table Relations](#table-relations)
    - [Things not worked on](#things-not-worked-on)
    - [Testing](#testing)
## Intro

A Ruby on Rails API which allows searching for restaurants and returning the best matches according to a few criteria (not documented here).

The application provides a single endpoint, `/api/restaurants/search` and it accepts the following parameters:

* `name`, a string, the name of a restaurant. Can be a partial match but not a fully fuzzy match.
* `rating`, an integer between 1 and 5, inclusive.
* `distance`, an integer between 1 and 10, inclusive.
* `price`, a a decimal number.
* `cuisine`, the name of a cuisine.

The only accepted HTTP verb is GET. The only format that API responds to is JSON. An example of a valid search query would be:

```
GET /api/restaurants/search?name=dish&rating=2&distance=4&price=35&cuisine=Malay
```

Missing parameters are ignored. Invalid parameters cause an error and the API returns a generic error message. See [things not worked on](#things-not-worked-on) for more details.

## Testing and development

**IMPORTANT**: All shell commands related to Rails assume that you have either exported the this shell environment variable:

```
$ export DB_LOCAL_PASSWORD=s3cr3t
```

Or that you prefix every Rails command with it, e.g:

```
$ DB_LOCAL_PASSWORD=s3cr3t bin/rails server
```

If Rails commands complain it cannot find that env var, try `spring stop` and run the command again.

### Cloning the repository and installing dependencies

Assuming you have `Ruby on Rails` and `bundler` installed, run:

```
$ git clone git@gitlab.com:fernandobasso-dev-challenges/alphasearch.git
$ cd alphasearch
$ bundle install
```

### Create the PostgreSQL role and databases

Create a PostgreSQL role for local testing and development. Log in as the role ‘postgres’ (or some other role with admin privileges) and run:

```
CREATE ROLE devel
WITH LOGIN PASSWORD 's3cr3t'
CREATEDB REPLICATION
VALID UNTIL 'infinity';
\q
```

From the shell (bash/zsh, etc.), log into `psql` with the new ‘devel’ role and create the databases for test and development:

```
$ psql -h localhost -U devel -d postgres -W
```

From `psql`, create the databases:

```
CREATE DATABASE alphasearch_devel WITH
  ENCODING='UTF8'
  OWNER devel
  LC_CTYPE='en_US.UTF-8'
  LC_COLLATE='en_US.UTF-8'
  TEMPLATE=template0
  CONNECTION LIMIT=5;

CREATE DATABASE alphasearch_test WITH
  ENCODING='UTF8'
  OWNER devel
  LC_CTYPE='en_US.UTF-8'
  LC_COLLATE='en_US.UTF-8'
  TEMPLATE=template0
  CONNECTION LIMIT=5;
```

At this point you should be able to run the migrations, which actually creates the tables:

```
$ DB_LOCAL_PASSWORD=s3cr3t bin/rails db:migrate
```

### Import CSV data into devel database

Import the “.csv” data into the devel database to try some searches with Postman, Insomnia, cURL or some other tool:

```
cd _stuff/

sed -n '2,$p' < ./cuisines.csv | \
  psql -h localhost -U devel -d alphasearch_devel -W -c \
  "\copy api_cuisines FROM STDIN DELIMITER ',' csv;"

awk -F, -v OFS=, 'NR>1 { sub(/\r$/, ""); print NR - 1,$5,$1,$2,$3,$4}' \
  < restaurants.csv | \
  psql -h localhost -U devel -d alphasearch_devel -W -c \
  "\copy api_restaurants FROM STDIN DELIMITER ',' csv;"
```

### Local Server and Testing

Start the local development server:

```
$ bin/rails server
```

And try to query some data:

```
$ curl --request GET 'http://localhost:3000/api/restaurants/search?name=dish' \
  --header 'Content-Type: application/json' | \
  jq -C | less -R
```

Running the controller tests:

```
$ bin/rails test test/controllers/
```

## Assumptions and further improvements

### Table Relations

As it was said that it could be assumed that a restaurant offers a single cuisine, it seemed OK to use an association that “a restaurant _belongs to_ a cuisine.” Perhaps “a restaurant _has one_ cuisine” would work too, but since the ‘restaurants.csv’ has `cuisine_id`, I decided to go with the first option.

### Things not worked on

Given the time allotted, I felt inclined to focus on the main functionality and implement basic search with the five criteria and at least a few controller tests to assert that at least some of the criteria is being met. That said, I did not work on:

* Validation of input and handling of errors. I did not validate the ranges if allowed inputs for rating, distance, etc. I just did a very rudimentary check and respond with a generic message if the query parameters are invalid.
* Documenting the API (with OpenAPI or something like that);
* Security;
* Authentication and Authorization;
* Performance;
* System or end to end tests (with Capybara or something similar);
* Create some shell scripts or rake tasks to automate the process of creating the databases and populating them with data from the .csv files or convert them into rails seeds.

These are the main things I would consider next given enough time.

### Testing

I spent a little bit of time to write a few tests for the `RestuarantsController` to check for the most basic queries and I did not fully exercise and exhaust all possibilities, which would require a more thorough examination of the possible combination of present and missing parameters in the search query.

