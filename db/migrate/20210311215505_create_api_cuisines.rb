class CreateApiCuisines < ActiveRecord::Migration[6.1]
  def change
    create_table :api_cuisines do |t|
      t.string :name, limit: 64
    end
  end
end
