class Api::RestaurantsController < ApplicationController
  ##
  # Perform a search on restaurants according to five criteria.
  #
  def search
    begin
      @restaurants = Api::Restaurant.search(search_params)

      respond_to do |f|
        f.json {
          render json: @restaurants, include: :cuisine
        }

        ##
        # Responses for any other format are not implemented yet.
        #
        f.any {
          render plain: '501 Not Implemented', status: :not_implemented
        }
      end
    rescue Exception
      render({
        :json => { error: '422 Unprocessable Entity. Check search parameters.' },
        :status => :unprocessable_entity
      })
    end
  end

  private
    def search_params
      ##
      # We accept at most these specific parameters.
      #
      params.permit(:name, :rating, :distance, :price, :cuisine)
    end
end
