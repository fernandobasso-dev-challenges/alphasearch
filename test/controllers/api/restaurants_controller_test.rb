require "test_helper"

class Api::RestaurantsControllerTest < ActionDispatch::IntegrationTest
  def get_json(q = '')
    get "#{api_restaurants_search_url}?#{q}", as: :json
    data = JSON.parse(response.body)
  end

  test "should get search" do
    get_json
    assert_response :success
  end

  test "should handle unprocessable parameters" do
    data = get_json 'rating=five'
    assert_response :unprocessable_entity
  end

  test "should never return more than five results" do
    data = get_json
    assert_operator data.length, :<=, 5
  end

  test "should search by name" do
    data = get_json 'name=dish'
    assert_response :success
    assert_equal 'Six Dishful', data[0]['name']
    assert_equal 'Three Dishy', data[1]['name']
  end

  test "should return empty results with inexisting name" do
    data = get_json 'name=sp34k-1337'
    assert_equal [], data
  end

  test "should return based on distance" do
    data = get_json 'distance=1'
    assert_equal 'Six Dishful', data.first['name']
    assert_operator data.length, :==, 1
  end

  test "should return based on price" do
    data = get_json 'price=5.99'
    assert_equal 'Two Dishless', data.first['name']
    assert_operator data.length, :==, 1
  end

  test "should return based on the cuisine" do
    data = get_json 'cuisine=malaysia'
    assert_operator data.length, :==, 4
    assert_equal 'Six Dishful', data.first['name']
    assert_equal 'One Dishful', data.last['name']
  end
end
