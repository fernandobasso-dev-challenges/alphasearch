class Api::Restaurant < ApplicationRecord
  belongs_to :cuisine
  validates :rating, numericality: true, allow_nil: true

  LIMIT = 5

  default_scope {
    joins(:cuisine)
      .order(rating: :desc, distance: :asc, price: :asc)
      .limit(LIMIT)
  }

  scope :with_name, -> (name) {
    where('api_restaurants.name ILIKE ?', "%#{name}%") if name.present?
  }

  scope :with_rating_gte, -> (rating) {
    where('rating >= ?', rating) if rating.present?
  }

  scope :with_distance_lte, -> (distance) {
    where('distance <= ?', distance) if distance.present?
  }

  scope :with_price_lte, -> (price) {
    where('price <= ?', price) if price.present?
  }

  scope :with_cuisine, -> (cuisine) {
    joins(:cuisine)
      .where('api_cuisines.name ILIKE ?', "%#{cuisine}%") if cuisine.present?
  }

  class << self
    def search(args)
      with_name(args[:name])
        .with_rating_gte(args[:rating])
        .with_distance_lte(args[:distance])
        .with_price_lte(args[:price])
        .with_cuisine(args[:cuisine])
    end
  end
end
